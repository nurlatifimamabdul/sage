<header class="banner">
  <div class="container">
    <?php
      if ( is_front_page() ) : ?>
        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <?php $description = get_bloginfo( 'description', 'display' ); ?>
        <?php if ( $description || is_customize_preview() ) : ?>
          <p class="site-description"><?php echo $description; ?></p>
        <?php endif; ?>
      <?php else : ?>
        <p class="none"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
      <?php endif; ?>

    <nav class="nav-primary">
      <div id="mainmenu">
        <?php wp_nav_menu( array( 'primary_navigation' => __('Primary Navigation', 'sage') ) ); ?>
      </div><!-- #mainmenu-->
      <div class="row-end"></div>
    </nav>

  </div>
</header>
